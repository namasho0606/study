class AddStartTimeToMicroposts < ActiveRecord::Migration[6.0]
  def change
    add_column :microposts, :start_time, :datetime

    remove_column :microposts, :subject, :string
  end
end
