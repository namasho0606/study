class RemoveCategoryIdToMicroposts < ActiveRecord::Migration[6.0]
  def change
    remove_column :microposts, :category_id
  end
end
