class AddsubjectToMicroposts < ActiveRecord::Migration[6.0]
  def change
    add_column :microposts, :subject, :string
  end
end
