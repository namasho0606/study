class ChangeTimeToMicropost < ActiveRecord::Migration[6.0]
  def change
    change_column :microposts, :time, :float
  end
end
