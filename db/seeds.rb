# メインのサンプルユーザーを1人作成する
User.create!(username: "Example User",
             email: "example@railstutorial.org",
             password: "hogehoge",
             password_confirmation: "hogehoge",
             confirmed_at: Time.now)

# 追加のユーザーをまとめて生成する
40.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(username: name,
               email: email,
               password: password,
               password_confirmation: password,
               confirmed_at: Time.now)
end

#カテゴリーリレーションシップを作成
users = User.order(:created_at).take(6)
users.each { |user|
  CATEGORIES.each { |category| user.categories.create!(name: category) }
}

# マイクロポストを作成
users = User.order(:created_at).take(6)
12.times do
  users.each {|user|
    content = Faker::Lorem.sentence(word_count: 1)
    comment = Faker::Lorem.sentence(word_count: 1)
    time = rand(1..4)
    subject = Category.find(rand(1..5)).name
    s1 = Date.today.beginning_of_month
    s2 = Date.today.end_of_month
    start_time = Random.rand(s1..s2).to_s
    post = user.microposts.create!(content: content, time: time, subject: subject, start_time: start_time)
    post.comments.create!(content: comment, user_id: rand(1..6))
    post.likes.create!(user_id: rand(1..6))
  }
end

#リレーションシップを生成
users = User.all
user  = users.first
following = users[2..40]
followers = users[3..30]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }