class MicropostsController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy]
  before_action :correct_user, only: :destroy

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = '投稿しました'
      redirect_to root_url
    else
      @feed_items = current_user.feed.paginate(page: params[:page], per_page: 5)
      render 'pages/home'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = '投稿を削除しました'
    redirect_to request.referer || root_url
  end

  def show
    @micropost = Micropost.find(params[:id])
    return unless @micropost.comments

    @comment = Comment.new
    @comments = @micropost.comments
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content, :time, :start_time, :subject)
    end

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
