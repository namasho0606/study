class CategoriesController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy]
  before_action :correct_user, only: [:destroy]

  def index
    @category = Category.new
    @categories = current_user.categories
  end

  def create
    @category = current_user.categories.build(category_params)
    if @category.save
      flash[:success] = 'カテゴリを追加しました'
      redirect_to categories_path
    else
      @categories = Category.where(user_id: current_user.id)
      render 'index'
    end
  end

  def destroy
    @category.destroy
    flash[:success] = 'カテゴリを削除しました'
    redirect_to categories_path
  end

  private

    def correct_user
      @category = current_user.categories.find_by(id: params[:id])
      redirect_to categories_path if @category.nil?
    end

    def category_params
      params.require(:category).permit(:name, :user_id)
    end
end
