RSpec.describe 'UserAuthentications', type: :request do
  let(:user) { create(:user) }
  let(:user_params) { attributes_for(:user) }
  let(:invalid_user_params) { attributes_for(:user, username: '') }

  describe 'POST #create' do
    before { ActionMailer::Base.deliveries.clear }

    context 'パラメータが妥当な場合' do
      it 'リクエストが成功すること' do
        post user_registration_path, params: { user: user_params }
        expect(response.status).to eq 302
      end

      it '認証メールが送信されること' do
        post user_registration_path, params: { user: user_params }
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end

      it 'createが成功すること' do
        expect do
          post user_registration_path, params: { user: user_params }
        end.to change(User, :count).by 1
      end

      it 'リダイレクトされること' do
        post user_registration_path, params: { user: user_params }
        expect(response).to redirect_to root_url
      end
    end

    context 'パラメータが不正な場合' do
      it 'リクエストが成功すること' do
        post user_registration_path, params: { user: invalid_user_params }
        expect(response.status).to eq 200
      end

      it '認証メールが送信されないこと' do
        post user_registration_path, params: { user: invalid_user_params }
        expect(ActionMailer::Base.deliveries.size).to eq 0
      end

      it 'createが失敗すること' do
        expect do
          post user_registration_path, params: { user: invalid_user_params }
        end.not_to change(User, :count)
      end

      it 'エラーが表示されること' do
        post user_registration_path, params: { user: invalid_user_params }
        expect(response.body).to include 'エラーが発生したため ユーザ は保存されませんでした。'
      end
    end
  end

  describe 'GET #edit' do
    context 'ログインしている場合' do
      before do
        user.confirm
        sign_in user
      end

      it 'リクエストが成功すること' do
        get edit_user_registration_path
        expect(response.status).to eq 200
      end
    end

    context 'ゲストの場合' do
      it 'リダイレクトされること' do
        get edit_user_registration_path
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

  describe 'GET #show' do
    context 'ユーザーが存在しない場合' do
      it 'エラーが発生すること' do
        user_id = user.id
        user.destroy
        expect { get "/users/#{user_id}" }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end

  describe 'delete /logout' do
    it 'root_pathにリダイレクトされること' do
      post user_registration_path, params: { user: user_params }
      delete destroy_user_session_path
      expect(response).to redirect_to root_path
    end
  end
end
