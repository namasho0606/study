RSpec.describe 'Comments', type: :request do
  describe 'comments#create' do
    let(:micropost) { create(:micropost) }
    let(:post_request) { post micropost_comments_path(micropost) }

    context 'ログインしていない時' do
      it 'コメントの数が変わっていないこと' do
        expect { post_request }.to change(Comment, :count).by(0)
      end

      it 'ログインページにリダイレクトされること' do
        expect(post_request).to redirect_to new_user_session_path
      end
    end
  end
end
