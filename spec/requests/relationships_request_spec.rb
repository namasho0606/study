RSpec.describe 'Relationships', type: :request do
  describe 'Relationships#create' do
    let(:post_request) { post relationships_path }

    context 'ログインしていない時' do
      it 'リレーションシップの数が変わっていないこと' do
        expect { post_request }.to change(Relationship, :count).by(0)
      end

      it 'ログインページにリダイレクトされること' do
        expect(post_request).to redirect_to new_user_session_path
      end
    end
  end

  describe 'Relationships#destroy' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let(:delete_request) { delete relationship_path(other_user) }

    before { user.following << other_user }

    context 'ログインしていない時' do
      it 'リレーションシップの数が変わっていないこと' do
        expect { delete_request }.to change(Relationship, :count).by(0)
      end

      it 'ログインページにリダイレクトされること' do
        expect(delete_request).to redirect_to new_user_session_path
      end
    end
  end
end
