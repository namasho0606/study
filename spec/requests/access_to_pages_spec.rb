RSpec.describe 'Access to pages', type: :request do
  context 'GET #home' do
    before { get root_path }

    it '正常なレスポンスを返すこと' do
      expect(response).to have_http_status :ok
    end

    it 'Study App　であること' do
      expect(response.body).to include full_title('')
      expect(response.body).not_to include '| Study App'
    end
  end
end
