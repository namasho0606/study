RSpec.describe 'Categories', type: :request do
  describe 'create' do
    let!(:category) { attributes_for(:category) }
    let(:post_request) { post categories_path, params: { category: category } }

    context 'ログインしていないとき' do
      it 'カテゴリ追加後、投稿数が変わらないこと' do
        expect { post_request }.to change(Category, :count).by(0)
      end

      it 'カテゴリ追加失敗後、ログインページにリダイレクトすること' do
        expect(post_request).to redirect_to new_user_session_path
      end
    end

    context 'ログインしているとき' do
      let(:user) { create(:user) }

      let(:category) { attributes_for(:category) }
      let(:category_invalid) { attributes_for(:category, name: '') }
      let(:post_request) { post categories_path, params: { category: category } }
      let(:post_request_invalid) { post categories_path, params: { category: category_invalid } }

      before { sign_in user }

      it 'カテゴリ追加後、カテゴリ数が変わること' do
        expect { post_request }.to change(Category, :count).by(1)
      end

      it '追加失敗後、カテゴリ数が変わないこと' do
        expect { post_request_invalid }.to change(Category, :count).by(0)
      end
    end
  end

  describe 'destroy' do
    let!(:category) { create(:category) }
    let(:delete_request) { delete category_path(category) }

    context 'ログインしていないとき' do
      it '削除後、カテゴリ数が変わらないこと' do
        expect { delete_request }.to change(Category, :count).by(0)
      end

      it '削除失敗後、ログインページにリダイレクトすること' do
        expect(delete_request).to redirect_to new_user_session_path
      end
    end

    context '別ユーザーでログインしているとき' do
      let(:user) { create(:user) }

      before { sign_in user }

      it '削除後、カテゴリ数が変わらないこと' do
        expect { delete_request }.to change(Category, :count).by(0)
      end

      it '削除失敗後、categories_pathにリダイレクトすること' do
        expect(delete_request).to redirect_to categories_path
      end
    end

    context 'ログインしているとき' do
      let(:user) { category.user }

      before { sign_in user }

      it '削除後、カテゴリ数が変わること' do
        expect { delete_request }.to change(Category, :count).by(-1)
      end
    end
  end
end
