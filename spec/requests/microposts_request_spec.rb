RSpec.describe 'Microposts', type: :request do
  describe 'create' do
    let(:micropost) { attributes_for(:micropost) }
    let(:post_request) { post microposts_path, params: { micropost: micropost } }

    context 'ログインしていないとき' do
      it '投稿後、投稿数が変わらないこと' do
        expect { post_request }.to change(Micropost, :count).by(0)
      end

      it '投稿失敗後、ログインページにリダイレクトすること' do
        expect(post_request).to redirect_to new_user_session_path
      end
    end
  end

  describe 'destroy' do
    let!(:micropost) { create(:micropost) }
    let(:delete_request) { delete micropost_path(micropost) }

    context 'ログインしていないとき' do
      it '削除後、投稿数が変わらないこと' do
        expect { delete_request }.to change(Micropost, :count).by(0)
      end

      it '削除失敗後、ログインページにリダイレクトすること' do
        expect(delete_request).to redirect_to new_user_session_path
      end
    end

    context '別ユーザーでログインしているとき' do
      let(:user) { create(:user) }

      before { sign_in user }

      it '削除後、投稿数が変わらないこと' do
        expect { delete_request }.to change(Micropost, :count).by(0)
      end

      it '削除失敗後、rootにリダイレクトすること' do
        expect(delete_request).to redirect_to root_path
      end
    end
  end
end
