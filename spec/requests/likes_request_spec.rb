RSpec.describe 'Likes', type: :request do
  describe 'likes#create' do
    let(:post_request) { post likes_path }

    context 'ログインしていない時' do
      it 'いいねの数が変わっていないこと' do
        expect { post_request }.to change(Like, :count).by(0)
      end

      it 'ログインページにリダイレクトされること' do
        expect(post_request).to redirect_to new_user_session_path
      end
    end
  end

  describe 'likes#destroy' do
    let(:micropost) { create(:micropost) }
    let(:other_user) { create(:user) }
    let(:delete_request) { delete like_path(micropost.user) }

    before { micropost.iine(other_user) }

    context 'ログインしていない時' do
      it 'いいねの数が変わっていないこと' do
        expect { delete_request }.to change(Like, :count).by(0)
      end

      it 'ログインページにリダイレクトされること' do
        expect(delete_request).to redirect_to new_user_session_path
      end
    end
  end
end
