RSpec.describe 'Users', type: :request do
  describe 'before_action: :logged_in_user' do
    let(:user) { create(:user) }

    it 'フォローイングページにアクセスするとリダイレクトされること' do
      get following_user_path(user)
      expect(response).to redirect_to new_user_session_path
    end

    it 'フォロワーページにアクセスするとリダイレクトされること' do
      get followers_user_path(user)
      expect(response).to redirect_to new_user_session_path
    end
  end
end
