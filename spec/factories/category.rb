FactoryBot.define do
  factory :category do
    name { 'category' }
    association :user
  end
end
