FactoryBot.define do
  factory :micropost do
    content { 'micropost test' }
    time { 3.0 }
    start_time { Time.zone.now }
    subject { 'subject' }

    association :user
  end

  trait :yesterday do
    content { 'yesterday' }
    time { 3.0 }
    start_time { 1.day.ago }
    subject { 'subject' }

    created_at { 1.day.ago }
  end

  trait :day_before_yesterday do
    content { 'day_before_yesterday' }
    time { 3.0 }
    start_time { 2.days.ago }
    subject { 'subject' }

    created_at { 2.days.ago }
  end

  trait :now do
    content { 'now!' }
    time { 3.0 }
    start_time { Time.zone.now }
    subject { 'subject' }

    created_at { Time.zone.now }
  end
end
