FactoryBot.define do
  factory :user, class: 'User' do
    sequence(:email) { |n| "TEST#{n}@example.com" }
    sequence(:username) { |n| "test_#{n}" }
    password { 'hogehoge' }
    password_confirmation { 'hogehoge' }
    confirmed_at { Time.zone.today }
  end

  factory :login_user, class: 'User' do
    email { 'login@example.com' }
    username { 'login' }
    password { 'hogehoge' }
    password_confirmation { 'hogehoge' }
    confirmed_at { Time.zone.today }
  end
end
