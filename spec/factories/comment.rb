FactoryBot.define do
  factory :comment do
    content { 'comment test' }
    micropost
    user
  end
end
