RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title(page_title)' do
    context 'page_title = emptyの時' do
      it 'Study App　であること' do
        expect(full_title('')).to eq 'Study App'
      end
    end

    context 'page_title = nilの時' do
      it 'Study App　であること' do
        expect(full_title(nil)).to eq 'Study App'
      end
    end

    context 'page_title = 文字列の時' do
      it '文字列 | Study App　であること' do
        expect(full_title('rails')).to eq 'rails | Study App'
      end
    end
  end
end
