RSpec.describe User, type: :model do
  describe 'validations' do
    let!(:user) { create(:user, email: 'original@example.com') }
    let(:user2) { create(:user, email: 'BIG@EXAMPLE.COM') }

    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_length_of(:username).is_at_most(30) }
    it { is_expected.to validate_length_of(:password).is_at_least(6) }

    it 'emailの一意性が正常に動作していること' do
      user = build(:user, email: 'original@example.com')
      expect(user).not_to be_valid
      user = build(:user, email: 'ORIGINAL@EXAMPLE.COM')
      expect(user).not_to be_valid
    end

    it 'emailのダウンケースが正常に動作していること' do
      expect(user2.reload.email).to eq 'big@example.com'
    end

    it 'passwordが空白の場合、失敗すること' do
      user = build(:user, password: ' ' * 6)
      expect(user).not_to be_valid
    end
  end

  describe 'dependent: :destroy' do
    let(:user) { create(:user) }

    before do
      user.save
      user.microposts.create!(content: 'Lorem ipsum', time: 3, start_time: Time.zone.today, subject: 'subject')
    end

    it 'ユーザーが削除された場合、関連するmicropostも削除されること' do
      expect do
        user.destroy
      end.to change(Micropost, :count).by(-1)
    end
  end

  describe 'follow and unfollow' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }

    before { user.follow(other_user) }

    describe 'フォロー' do
      it 'フォローが成功していること' do
        expect(user.following?(other_user)).to be_truthy
      end

      it 'other_userがフォロワーズに含まれていること' do
        expect(other_user.followers.include?(user)).to be_truthy
      end
    end

    describe 'アンフォロー' do
      it 'アンフォローが成功すること' do
        user.unfollow(other_user)
        expect(user.following?(other_user)).to be_falsy
      end
    end
  end
end
