RSpec.describe Like, type: :model do
  it { is_expected.to validate_presence_of(:user_id) }
  it { is_expected.to validate_presence_of(:micropost_id) }

  describe 'validation' do
    let(:micropost) { create(:micropost) }
    let(:other_user) { create(:user) }
    let(:like) { Like.create(user_id: other_user.id, micropost_id: micropost.id) }

    it 'テストデータが有効であること' do
      expect(like).to be_valid
    end

    describe 'presence' do
      it 'user_idがないとき無効であること' do
        like.user_id = nil
        expect(like).to be_invalid
      end

      it 'micropost_idがないとき無効であること' do
        like.micropost_id = nil
        expect(like).to be_invalid
      end
    end
  end
end
