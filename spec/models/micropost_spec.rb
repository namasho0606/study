RSpec.describe Micropost, type: :model do
  let(:micropost) { create(:micropost) }

  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to validate_length_of(:content).is_at_most(140) }
  it { is_expected.to validate_presence_of(:time) }
  it { is_expected.to validate_presence_of(:start_time) }
  it { is_expected.to validate_presence_of(:subject) }

  it 'micropostが有効であること' do
    expect(micropost).to be_valid
  end

  it 'ユーザーがnilの場合、micropostが無効であること' do
    micropost.user_id = nil
    expect(micropost).to be_invalid
  end

  it 'contentが空白の場合、micropostが無効であること' do
    micropost.content = ' '
    expect(micropost).to be_invalid
  end

  it 'contentが141字以上の場合、micropostが無効であること' do
    micropost.content = 'a' * 141
    expect(micropost).to be_invalid
  end

  it 'timeがnilの場合、micropostが無効であること' do
    micropost.time = nil
    expect(micropost).to be_invalid
  end

  it 'start_timeがnilの場合、micropostが無効であること' do
    micropost.start_time = nil
    expect(micropost).to be_invalid
  end

  it 'subjectがnilの場合、micropostが無効であること' do
    micropost.subject = nil
    expect(micropost).to be_invalid
  end

  describe 'micropostのソート' do
    let!(:day_before_yesterday) { create(:micropost, :day_before_yesterday) }
    let!(:now) { create(:micropost, :now) }
    let!(:yesterday) { create(:micropost, :yesterday) }

    it '投稿順であること' do
      expect(Micropost.first).to eq now
    end
  end
end
