RSpec.describe Relationship, type: :model do
  it { is_expected.to validate_presence_of(:follower_id) }
  it { is_expected.to validate_presence_of(:followed_id) }

  describe 'validation' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let(:relationship) { user.active_relationships.build(followed_id: other_user.id) }

    it 'テストデータが有効であること' do
      expect(relationship).to be_valid
    end

    describe 'presence' do
      it 'follower_idがないとき無効であること' do
        relationship.follower_id = nil
        expect(relationship).to be_invalid
      end

      it 'followed_idがないとき無効であること' do
        relationship.followed_id = nil
        expect(relationship).to be_invalid
      end
    end
  end
end
