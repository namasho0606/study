RSpec.describe Comment, type: :model do
  let(:comment) { create(:comment) }
  let(:other_user) { create(:user) }

  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to validate_length_of(:content).is_at_most(140) }

  it 'commentが有効であること' do
    expect(comment).to be_valid
  end

  it 'ユーザーがnilの場合、commentが無効であること' do
    comment.user_id = nil
    expect(comment).to be_invalid
  end

  it 'マイクロポストがnilの場合、commentが無効であること' do
    comment.micropost_id = nil
    expect(comment).to be_invalid
  end

  it 'contentが空白の場合、micropostが無効であること' do
    comment.content = ' '
    expect(comment).to be_invalid
  end

  it 'contentが141字以上の場合、commentが無効であること' do
    comment.content = 'a' * 141
    expect(comment).to be_invalid
  end
end
