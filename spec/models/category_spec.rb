RSpec.describe Category, type: :model do
  let(:category) { create(:category) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name).is_at_most(10) }

  it 'categoryが有効であること' do
    expect(category).to be_valid
  end

  it 'ユーザーがnilの場合、micropostが無効であること' do
    category.user_id = nil
    expect(category).to be_invalid
  end

  it 'nameが空白の場合、categoryが無効であること' do
    category.name = ' '
    expect(category).to be_invalid
  end

  it 'nameが10字以上の場合、categoryが無効であること' do
    category.name = 'a' * 11
    expect(category).to be_invalid
  end
end
