RSpec.describe 'UsersEdits', type: :system do
  let(:login_user) { create(:login_user) }

  before do
    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
    click_link 'マイページ'
    click_link '編集'
  end

  it '不適切なユーザーフォームの場合失敗すること' do
    within('#edit_user') do
      fill_in 'ユーザーネーム', with: ''
      fill_in 'メールアドレス', with: 'user@invalid'
      fill_in 'パスワード', with: 'foo'
      fill_in '確認用パスワード', with: 'bar'
      fill_in '現在のパスワード', with: 'hogehoge'
      click_on '更新'
    end
    aggregate_failures do
      expect(has_css?('.alert-danger')).to be_truthy
    end
  end

  it '適切なユーザーフォームの場合正常なレスポンスを返すこと' do
    within('#edit_user') do
      fill_in 'ユーザーネーム', with: 'FooBar'
      fill_in 'メールアドレス', with: 'foo@bar.com'
      fill_in 'パスワード', with: ''
      fill_in '確認用パスワード', with: ''
      fill_in '現在のパスワード', with: 'hogehoge'
      click_on '更新'
    end
    aggregate_failures do
      expect(has_css?('.alert-notice')).to be_truthy
    end
  end
end
