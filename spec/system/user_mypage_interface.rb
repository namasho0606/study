RSpec.describe 'UserMypagesInterfaces', type: :system do
  let(:login_user) { create(:login_user) }

  before do
    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
    # カテゴリの追加
    click_link 'カテゴリー'
    valid_name = 'subject'
    fill_in 'category_name', with: valid_name
    click_on '追加'
    # マイクロポストの追加
    click_link 'Home'
    valid_content = 'This micropost really ties the room together'
    within('#micropost_form') do
      fill_in 'micropost[time]', with: 2
      select 'subject', from: 'micropost_subject'
      fill_in 'micropost_content', with: valid_content
      fill_in 'micropost_start_time', with: Time.zone.today
      find('.btn-submit').click
    end
    find '.alert', text: '投稿しました'
  end

  it 'user_mypage interface' do
    click_link 'マイページ'
    expect(page).to have_current_path user_path(login_user), ignore_query: true
    # 　カレンダーに当日の勉強時間が記載されていること
    within('.table-striped') do
      expect(page).to have_selector '.today', text: '2.0時間'
    end
    expect(page).to have_selector '.study-time', text: '勉強総計：2.0　時間'
  end
end
