RSpec.describe 'SiteLayout', type: :system do
  context 'access to root_path' do
    before { visit root_path }

    it 'root_pathへのlinkがあること' do
      expect(page).to have_link nil, href: root_path, count: 2
    end
  end

  context 'access to user_session_path' do
    before { visit user_session_path }

    it "'ログイン'のcontentとタイトルを持つこと" do
      expect(page).to have_content 'ログイン'
      expect(page).to have_title full_title('ログイン')
    end
  end
end
