RSpec.describe 'CategoriesInterfaces', type: :system do
  let(:login_user) { create(:login_user) }
  let(:category) { create(:category) }

  before do
    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
  end

  it 'category interface' do
    click_link 'カテゴリー'

    # 無効な送信
    click_on '追加'
    expect(has_css?('.alert-danger')).to be_truthy

    # 有効な送信
    valid_name = 'subject'
    fill_in 'category_name', with: valid_name
    expect do
      click_on '追加'
      expect(page).to have_current_path categories_path, ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
    end.to change(Category, :count).by(1)

    # 投稿を削除する
    expect do
      page.accept_confirm do
        all('ol li')[0].click_on '削除'
      end
      expect(page).to have_current_path categories_path, ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
    end.to change(Category, :count).by(-1)
  end
end
