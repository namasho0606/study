RSpec.describe 'Likes', type: :system do
  let(:micropost) { create(:micropost) }
  let(:login_user) { create(:login_user) }
  let(:other_users) { create(:user) }

  before do
    micropost.iine(other_users)

    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
  end

  it 'いいねの数が正しいこと' do
    expect(micropost.likes.count).to eq 1
  end

  it 'いいねでいいね数が増えていること' do
    expect do
      visit user_path(micropost.user)
      find('.btn-xs').click
      # Ajaxの処理待ちの為に入れています
      visit user_path(micropost.user)
    end.to change(micropost.likes, :count).by(1)
  end

  it 'いいね実行からいいね取り消しでいいね数が変わっていないこと' do
    visit user_path(micropost.user)
    expect do
      find('.btn-xs').click
      visit user_path(micropost.user)
      find('.btn-xs').click
      visit user_path(micropost.user)
    end.to change(micropost.likes, :count).by(0)
  end
end
