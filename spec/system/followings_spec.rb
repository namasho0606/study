RSpec.describe 'Followings', type: :system do
  let(:login_user) { create(:login_user) }
  let(:other_users) { create_list(:user, 20) }

  before do
    other_users[0..9].each do |other_user|
      login_user.active_relationships.create!(followed_id: other_user.id)
      login_user.passive_relationships.create!(follower_id: other_user.id)
    end
    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
  end

  it 'フォロワー数とフォロー数が正しいこと' do
    click_link 'Home'
    click_link 'following'
    expect(login_user.following.count).to eq 10
    login_user.following.each do |u|
      expect(page).to have_link u.username, href: user_path(u)
    end

    click_link 'followers'
    expect(login_user.followers.count).to eq 10
    login_user.followers.each do |u|
      expect(page).to have_link u.username, href: user_path(u)
    end
  end

  it 'アンフォローでフォローイング数が減っていること' do
    visit user_path(other_users.first.id)
    expect do
      click_on 'Unfollow'
      expect(page).not_to have_link 'Unfollow'
      # Ajaxの処理待ちの為に入れています
      visit current_path
    end.to change(login_user.following, :count).by(-1)
  end

  it 'フォローでフォローイング数が増えていること' do
    visit user_path(other_users.last.id)
    expect do
      click_on 'Follow'
      expect(page).not_to have_link 'Follow'
      # Ajaxの処理待ちの為に入れています
      visit current_path
    end.to change(login_user.following, :count).by(1)
  end
end
