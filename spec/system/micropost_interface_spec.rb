RSpec.describe 'MicropostsInterfaces', type: :system do
  let(:login_user) { create(:login_user) }
  let(:micropost) { create(:micropost) }

  before do
    10.times do
      content = Faker::Lorem.sentence(word_count: 1)
      time = 2
      start_time = Time.zone.today
      subject = 'subject'
      login_user.microposts.create!(content: content, time: time, start_time: start_time, subject: subject)
    end

    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
    # カテゴリの追加
    click_link 'カテゴリー'
    valid_name = 'subject'
    fill_in 'category_name', with: valid_name
    click_on '追加'
  end

  it 'micropost interface' do
    click_on 'Home'

    # 無効な送信
    click_on '投稿'
    expect(has_css?('.alert-danger')).to be_truthy

    # 正しいページネーションリンク
    click_on '2'
    expect(URI.parse(current_url).query).to eq 'page=2'

    # 有効な送信
    valid_content = 'This micropost really ties the room together'
    within('#micropost_form') do
      fill_in 'micropost[time]', with: 2
      select 'subject', from: 'micropost_subject'
      fill_in 'micropost_content', with: valid_content
      fill_in 'micropost_start_time', with: Time.zone.today
    end
    expect do
      find('.btn-submit').click
      find '.alert', text: '投稿しました'
      expect(page).to have_current_path root_path, ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
      visit root_path
    end.to change(Micropost, :count).by(1)

    # 投稿を削除する
    expect do
      page.accept_confirm do
        all('ol li')[0].click_on '削除'
      end
      find '.alert', text: '投稿を削除しました'
      expect(page).to have_current_path root_path, ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
    end.to change(Micropost, :count).by(-1)

    # 違うユーザのプロフィールにアクセス(削除リンクがないことを確認)
    visit user_path(micropost.user)
    expect(page).not_to have_link '削除'
  end
end
