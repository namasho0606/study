RSpec.describe 'UsersSignup', type: :system do
  before do
    ActionMailer::Base.deliveries.clear
  end

  def extract_confirmation_url(mail)
    body = mail.body.encoded
    body[/http[^"]+/]
  end

  it 'ユーザー登録を行い、ログイン　ログアウトを行う' do
    visit root_path
    within('.home-content') do
      click_link '新規登録'
    end
    expect(page).to have_current_path new_user_registration_path, ignore_query: true
    visit new_user_registration_path
    within('#new_user') do
      fill_in 'ユーザーネーム', with: 'FooBar'
      fill_in 'メールアドレス', with: 'foobar@foobar.com'
      fill_in 'パスワード', with: 'hogehoge'
      fill_in '確認用パスワード', with: 'hogehoge'
      expect { click_button 'アカウント登録' }.to change { ActionMailer::Base.deliveries.size }.by(1)
    end
    expect(page).to have_content '本人確認用のメールを送信しました。メール内のリンクからアカウントを有効化させてください。'

    user = User.last
    token = user.confirmation_token
    visit user_confirmation_path(confirmation_token: token)
    expect(page).to have_content 'メールアドレスが確認できました。'
    # ログアウト
    click_link 'ログアウト'
    expect(page).to have_content 'ログアウトしました。'
    # 登録に失敗する場合
    visit root_path
    click_link 'ログイン'
    click_button 'ログイン'
    expect(page).to have_content 'メールアドレスまたはパスワードが違います。'
    fill_in 'パスワード', with: '12345678'
    click_button 'ログイン'
    expect(page).to have_content 'メールアドレスまたはパスワードが違います。'
    # 登録に成功する場合
    fill_in 'メールアドレス', with: 'foobar@foobar.com'
    fill_in 'パスワード', with: 'hogehoge'
    click_button 'ログイン'
    expect(page).to have_content 'ログインしました。'
  end
end
