RSpec.describe 'CommentsInterfaces', type: :system do
  let(:login_user) { create(:login_user) }
  let(:micropost) { create(:micropost) }

  before do
    visit new_user_session_path
    fill_in 'メールアドレス', with: login_user.email
    fill_in 'パスワード', with: login_user.password
    click_button 'ログイン'
  end

  it 'comment interface' do
    visit micropost_path(micropost)

    # 無効な送信
    click_on 'コメント'
    expect(has_css?('.alert-danger')).to be_truthy

    # 有効な送信
    valid_content = 'This micropost really ties the room together'
    fill_in 'comment_content', with: valid_content
    expect do
      click_on 'コメント'
      expect(page).to have_current_path micropost_path(micropost), ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
    end.to change(Comment, :count).by(1)

    # 投稿を削除する
    expect do
      page.accept_confirm do
        click_on '削除'
      end
      expect(page).to have_current_path micropost_path(micropost), ignore_query: true
      expect(has_css?('.alert-success')).to be_truthy
    end.to change(Comment, :count).by(-1)
  end
end
