Rails.application.routes.draw do
  root 'pages#home'
  devise_for :users, :controllers => {
    :confirmations => 'users/confirmations'
  }
  resources :users, only: %i[index show] do
    member do
      get :following, :followers
    end
  end
  resources :microposts, only: [:create, :destroy, :show] do
    resources :comments, only: [:create, :destroy]
  end
  resources :relationships,       only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
  resources :categories, only: [:create, :destroy, :index]
end
